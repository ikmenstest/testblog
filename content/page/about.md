---
title: About this blog
subtitle: Waarom een test blog?
comments: false
---

Een test blog voor als ik iets fout doe en het niet op tijd kan verbeteren.

Dit is mijn normale [blog](https://ikmens.gitlab.io/tysblog/)